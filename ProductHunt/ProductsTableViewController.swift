//
//  ProductsTableViewController.swift
//  ProductHunt
//
//  Created by  Alexander Saprykin on 26.01.17.
//  Copyright © 2017 MostWanted. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductsTableViewController: UITableViewController, CategoriesTableViewControllerDelegate {
    
    var category = Categories()
    var posts = [Posts]()
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Tech"
        category.setSlug(slug: "tech")
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(ProductsTableViewController.refresh(sender:)), for: .valueChanged)
        
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        if isLoading {
            
            posts.removeAll()
            loadData()
            isLoading = false
        }
    }
    
    func loadData() {
        
        let url = "https://api.producthunt.com/v1/posts/all"
        let params = ["access_token": "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff", "search[category]": category.getSlug()]
        
        Alamofire.request(url, method: .get, parameters: params).validate().responseJSON { (response) in
            if let value = response.result.value {
                let json = JSON(value)
                
                for (_, subJSON): (String, JSON) in json["posts"] {
                    
                    let post = Posts()
                    
                    post.setName(name: subJSON["name"].stringValue)
                    post.setDescriprion(descriprion: subJSON["tagline"].stringValue)
                    post.setVotesCount(votesCount: subJSON["votes_count"].intValue)
                    post.setThumbnail(thumbnail: subJSON["thumbnail"]["image_url"].stringValue)
                    post.setURL(url: subJSON["discussion_url"].stringValue)
                    post.setScreenshotURL(screenshotURL: subJSON["screenshot_url"]["850px"].stringValue)
                    
                    self.posts.append(post)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func refresh(sender:AnyObject) {
        loadData()
        self.refreshControl?.endRefreshing()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return posts.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        let nameLabel = cell.viewWithTag(2) as! UILabel
        let descriptionTextView = cell.viewWithTag(3) as! UITextView
        let votesCount = cell.viewWithTag(4) as! UILabel
        
        nameLabel.text = "Name: " + posts[indexPath.row].getName()
        descriptionTextView.text = posts[indexPath.row].getDescriprion()
        votesCount.text = "Likes: " + String(posts[indexPath.row].getVotesCount())
        
        let url = URL(string: posts[indexPath.row].getThumbnail())
        let data = try? Data(contentsOf: url!)
        imageView.image = UIImage(data: data!)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "categorySegue" {
            let vc = segue.destination as! CategoriesTableViewController
            vc.delegate = self
        }
        
        if segue.identifier == "detailSegue" {
            let vc = segue.destination as! DetailViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                vc.post = posts[indexPath.row]
            }
        }
    }
    
    // MARK: - CategoriesTableViewControllerDelegate
    
    func sendCategory(category: Categories) {
        self.isLoading = true
        self.category = category
        self.title = category.getName()
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
