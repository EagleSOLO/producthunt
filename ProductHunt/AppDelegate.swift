//
//  AppDelegate.swift
//  ProductHunt
//
//  Created by  Alexander Saprykin on 26.01.17.
//  Copyright © 2017 MostWanted. All rights reserved.
//

import UIKit
import UserNotifications
import Alamofire
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
            if !accepted {
                print("Notification access denied.")
            }
        }
        UIApplication.shared.setMinimumBackgroundFetchInterval(600)

        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        newNotifications()
        completionHandler(.newData)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func newNotifications() {
        
        var notificationsID = [Int]()
        
        let url = "https://api.producthunt.com/v1/notifications"
        let params = ["access_token": "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff", "search[type]": "all"]
        
        Alamofire.request(url, method: .get, parameters: params).validate().responseJSON { (response) in
            if let value = response.result.value {
                let json = JSON(value)
                
                for (_, subJSON): (String, JSON) in json["notifications"] {
                    notificationsID.append(subJSON["id"].intValue)
                }
            }
        }
        
        if notificationsID.count == 1 {
            
            let content = UNMutableNotificationContent()
            content.body = "1 new product"
            content.categoryIdentifier = "message"
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0,repeats: false)
            let request = UNNotificationRequest(identifier: "requestMessage",content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        } else if notificationsID.count > 1 {
            
            let content = UNMutableNotificationContent()
            content.body = String(notificationsID.count) + " new products"
            content.categoryIdentifier = "message"
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0,repeats: false)
            let request = UNNotificationRequest(identifier: "requestMessage",content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
    }
}
