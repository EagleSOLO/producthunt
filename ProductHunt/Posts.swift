//
//  Posts.swift
//  ProductHunt
//
//  Created by  Alexander Saprykin on 26.01.17.
//  Copyright © 2017 MostWanted. All rights reserved.
//

import Foundation


class Posts {
    
    private var name = String()
    private var descriprion = String()
    private var votesCount = Int()
    private var thumbnail = String()
    private var url = String()
    private var screenshotURL = String()
    
    public func getName() -> String {
        return self.name
    }
    
    public func setName(name: String) {
        self.name = name
    }
    
    public func getDescriprion() -> String {
        return self.descriprion
    }
    
    public func setDescriprion(descriprion: String) {
        self.descriprion = descriprion
    }
    
    public func getVotesCount() -> Int {
        return self.votesCount
    }
    
    public func setVotesCount(votesCount: Int) {
        self.votesCount = votesCount
    }

    public func getThumbnail() -> String {
        return self.thumbnail
    }
    
    public func setThumbnail(thumbnail: String) {
        self.thumbnail = thumbnail
    }
    
    public func getURL() -> String {
        return self.url
    }
    
    public func setURL(url: String) {
        self.url = url
    }
    
    public func getScreenshotURL() -> String {
        return self.screenshotURL
    }
    
    public func setScreenshotURL(screenshotURL: String) {
        self.screenshotURL = screenshotURL
    }
}
