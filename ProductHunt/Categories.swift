//
//  Categories.swift
//  ProductHunt
//
//  Created by  Alexander Saprykin on 26.01.17.
//  Copyright © 2017 MostWanted. All rights reserved.
//

import Foundation


class Categories {
    
    private var id = Int()
    private var slug = String()
    private var name = String()
    
    public func getID() -> Int {
        return self.id
    }
    
    public func setID(id: Int) {
        self.id = id
    }
    
    public func getSlug() -> String {
        return self.slug
    }
    
    public func setSlug(slug: String) {
        self.slug = slug
    }
    
    public func getName() -> String {
        return self.name
    }
    
    public func setName(name: String) {
        self.name = name
    }
}
