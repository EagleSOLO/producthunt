//
//  CategoriesTableViewController.swift
//  ProductHunt
//
//  Created by  Alexander Saprykin on 26.01.17.
//  Copyright © 2017 MostWanted. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


protocol CategoriesTableViewControllerDelegate {
    func sendCategory(category: Categories)
}


class CategoriesTableViewController: UITableViewController {
    
    var categories = [Categories]()
    var delegate: CategoriesTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = "https://api.producthunt.com/v1/categories"
        let params = ["access_token": "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff"]
        
        Alamofire.request(url, method: .get, parameters: params).validate().responseJSON { (response) in
            if let value = response.result.value {
                let json = JSON(value)
                
                for (_, subJSON): (String, JSON) in json["categories"] {
                    
                    let category = Categories()
                    
                    category.setID(id: subJSON["id"].intValue)
                    category.setSlug(slug: subJSON["slug"].stringValue)
                    category.setName(name: subJSON["name"].stringValue)

                    self.categories.append(category)
                }
            }
            self.tableView.reloadData()
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categories.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        cell.textLabel?.text = categories[indexPath.row].getName()
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.sendCategory(category: categories[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
