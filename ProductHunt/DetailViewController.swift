//
//  DetailViewController.swift
//  ProductHunt
//
//  Created by  Alexander Saprykin on 28.01.17.
//  Copyright © 2017 MostWanted. All rights reserved.
//

import UIKit


class DetailViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var votesCountLabel: UILabel!
    @IBOutlet weak var descriptionTextArea: UITextView!
    @IBOutlet weak var screenshotImageView: UIImageView!
    
    var post = Posts()
    var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = post.getName()
        
        self.nameLabel.text = "Name: " + post.getName()
        self.votesCountLabel.text = "Likes: " + String(post.getVotesCount())
        self.descriptionTextArea.text = post.getDescriprion()
        
        let url = URL(string: post.getScreenshotURL())
        let data = try? Data(contentsOf: url!)
        
        guard data != nil else { return }
        
        self.screenshotImageView.image = UIImage(data: data!)
    }
    
    @IBAction func getItButton(_ sender: UIButton) {
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        if let url = URL(string: post.getURL()) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
}
